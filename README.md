**Simple Android Project for Machine learning**	
	This project is a basic android app developed which acts as an social media forum of foodies and most importantly people who dont have much much experience. Dont expect any rocket science in this project. 

**Home Screen**

![Scheme](imagesReadme/1.png)

**Add Comment Screen**

![Scheme](imagesReadme/2.png)

**View Recipie Screen**

![Scheme](imagesReadme/3.png)

**Navigation Option Screen**

![Scheme](imagesReadme/4.png)


**Add Recipie Screen**

![Scheme](imagesReadme/5.png)


---
