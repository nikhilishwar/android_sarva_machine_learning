package com.machinelearning.developer.machinelearning;

import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

public class AppConstant {
    public static final String PREFERANCE_NAME = "mYpreFarAnce";
    public static final String USER_NAME = "userName";
    public static final String USER_MOBILE_NUMBER = "mobilenumber";
    public static MainActivity mainActivity;
    public static String lastViewdContentId = "lastViewdContentId";
    static  String myUserMobileNumber = "";
    static String myName = "";
    public  static  String user_name = "";

    public static boolean isInRecipie = false;
    public static  void saveLastViewdContent(ListItem item, Context ctx){
        SharedPreferences editor = ctx.getSharedPreferences(AppConstant.PREFERANCE_NAME,ctx.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = editor.edit();
        Gson gson = new Gson();
        String json = gson.toJson(item);
        prefsEditor.putString(AppConstant.lastViewdContentId, json);
        prefsEditor.commit();
    }
    public static  ListItem getLastViewdContent( Context ctx){
        SharedPreferences editor = ctx.getSharedPreferences(AppConstant.PREFERANCE_NAME,ctx.MODE_PRIVATE);
        Gson gson = new Gson();
        String json = editor.getString(AppConstant.lastViewdContentId, "");
        ListItem obj = gson.fromJson(json, ListItem.class);
        return obj;
    }
    public static ListItem cachedListItem;
    public static RecipieFragment cachedRecipieFragment;
    public static  String fileWriter = "@relation Star_Rating_OnComment\n" +
            "\n" +
            "@attribute 'good' numeric\n" +
            "@attribute 'like'  numeric\n" +
            "@attribute 'dontlike' numeric\n" +
            "@attribute 'hate' numeric\n" +
            "@attribute 'worst' numeric\n" +
            "@attribute 'bad' numeric\n" +
            "@attribute 'verygood' numeric\n" +
            "@attribute 'tasty' numeric\n" +
            "@attribute 'yummy' numeric\n" +
            "@attribute 'best'  numeric\n" +
            "@attribute 'hot' numeric\n" +
            "@attribute 'cold' numeric\n" +
            "@attribute 'spicy' numeric\n" +
            "@attribute 'salty' numeric\n" +
            "@attribute 'expensive' numeric\n" +
            "@attribute 'love' numeric\n" +
            "@attribute 'likedit' numeric\n" +
            "@attribute 'shit' numeric\n" +
            "@attribute 'fuck' numeric\n" +
            "@attribute 'nonesense' numeric\n" +
            "@attribute 'sucks' numeric\n" +
            "@attribute 'trouble' numeric\n" +
            "@attribute 'disgusting' numeric\n" +
            "@attribute 'looksgood' numeric\n" +
            "@attribute 'tastesgood' numeric\n" +
            "@attribute 'class' {positive,negetive}\n" +
            "\n" +
            "@data\n";


}
