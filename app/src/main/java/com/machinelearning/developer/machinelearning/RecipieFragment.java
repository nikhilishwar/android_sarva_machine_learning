package com.machinelearning.developer.machinelearning;

import android.content.Intent;
import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

public class RecipieFragment extends Fragment {
    private RecyclerView recyclerView;
    private MyAdapter adapter;
    private List<ListItem> listItem;
    View view;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_recipie,container,false);
        view = v;
        AppConstant.cachedRecipieFragment = this;
        DatabaseHandler db = new DatabaseHandler(v.getContext());
        List<RecipieDataModel> allRecipie = db.getAllRecipieByMobileNumber(AppConstant.myUserMobileNumber);
        recyclerView = (RecyclerView)v.findViewById(R.id.recyclerview);
        recyclerView.setVisibility(View.VISIBLE);
        FloatingActionButton btn = (FloatingActionButton) v.findViewById(R.id.floatingActionButton2);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(RecipieFragment.this.view.getContext(),AddRecipie.class);
                RecipieFragment.this.startActivity(i);
            }
        });
        if (allRecipie == null){
            recyclerView.setVisibility(View.GONE);
            return  v;
        }
        listItem = new ArrayList<>();
        for (RecipieDataModel recipieModel: allRecipie){
            ListItem listIte = new ListItem(recipieModel.recipieName,recipieModel.recipieStar,recipieModel.recipieHead,
                    recipieModel.recipieFull,"no",recipieModel.recipieId);
            listItem.add(listIte);
        }
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        adapter = new MyAdapter(listItem,v.getContext());

        recyclerView.setAdapter(adapter);
        return v;
    }

    void refresh() {

        DatabaseHandler db = new DatabaseHandler(view.getContext());
        List<RecipieDataModel> allRecipie = db.getAllRecipieByMobileNumber(AppConstant.myUserMobileNumber);
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerview);
        recyclerView.setVisibility(View.VISIBLE);
        if (allRecipie == null){
            recyclerView.setVisibility(View.GONE);
            return;
        }
        if (listItem == null){
            listItem = new ArrayList<>();
        }
        listItem.clear();

        for (RecipieDataModel recipieModel: allRecipie){
            ListItem listIte = new ListItem(recipieModel.recipieName,recipieModel.recipieStar,recipieModel.recipieHead,
                    recipieModel.recipieFull,"no",recipieModel.recipieId);
            listItem.add(listIte);
        }
        if (adapter == null){

            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
            adapter = new MyAdapter(listItem,view.getContext());

            recyclerView.setAdapter(adapter);

            return;
        }
        adapter.updateAdapter(listItem);
    }

}
