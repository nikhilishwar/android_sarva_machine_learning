package com.machinelearning.developer.machinelearning;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "foodManager";
    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String CREATE_USER_TABLE = "CREATE TABLE login_data(user_mobileNumber TEXT PRIMARY KEY,user_password TEXT,user_name TEXT)";
        sqLiteDatabase.execSQL(CREATE_USER_TABLE);

        String CREATE_RECIP_TABLE = "CREATE TABLE reciepie(auto_id INTEGER PRIMARY KEY AUTOINCREMENT, user_mobileNumber TEXT,reciepie_star TEXT,reciepie_name TEXT,reciepie_description_head TEXT,reciepie_description_full TEXT,user_name TEXT)";
        sqLiteDatabase.execSQL(CREATE_RECIP_TABLE);

        String CREATE_COMENT_TABLE = "CREATE TABLE user_comment(auto_id INTEGER PRIMARY KEY AUTOINCREMENT, auot_id_recipie INTEGER,user_mobileNumber TEXT,user_name TEXT,comment_text TEXT)";
        sqLiteDatabase.execSQL(CREATE_COMENT_TABLE);
    }
    public DatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        //3rd argument to be passed is CursorFactory instance
    }

    void createComments(String idOfReciepe,String userMobileNumber,String userName,String commentText){
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("auot_id_recipie", idOfReciepe); // Contact Name
        values.put("user_mobileNumber", userMobileNumber); // Contact Phone
        values.put("user_name", userName);
        values.put("comment_text", commentText);
        db.insert("user_comment", null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }

    public List<CommentDataModel> getAllCommentByRecipie(String id) {
        List<CommentDataModel> contactList = new ArrayList<CommentDataModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM user_comment where auot_id_recipie=" + id;

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor == null)
            return  null;
        if (cursor.getCount() == 0){
            return  null;
        }
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                CommentDataModel contact = new CommentDataModel(cursor.getString(0),cursor.getString(1),cursor.getString(2),
                        cursor.getString(3),cursor.getString(4));

                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS login_data");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS reciepie");
        sqLiteDatabase.execSQL("DROP TABLE IF EXISTS user_comment");
        // Create tables again
        onCreate(sqLiteDatabase);
    }


    String createRecipie(String userMobileNumber,String recipieStar,String recipieName,String recipieHead,String recipieFull,String username) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put("user_mobileNumber", userMobileNumber); // Contact Name
        values.put("reciepie_star", recipieStar); // Contact Phone
        values.put("reciepie_name", recipieName);
        values.put("reciepie_description_head", recipieHead);

        values.put("user_name", username);
        values.put("reciepie_description_full", recipieFull);
        db.insert("reciepie", null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
       return getAllRecipieLastId();
    }

    public String getAllRecipieLastId() {
        String max = "1";
        // Select All Query
        String selectQuery = "SELECT  max(auto_id) as auto_id FROM reciepie";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor == null)
            return  null;
        if (cursor.getCount() == 0){
            return  null;
        }
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
               max =  cursor.getString(0);

                // Adding contact to list

            } while (cursor.moveToNext());
        }

        // return contact list
        return max;
    }

    public int updateRecipie(String id,String userMobileNumber,String recipieStar,String recipieName,String recipieHead,String recipieFull) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("user_mobileNumber", userMobileNumber); // Contact Name
        values.put("reciepie_star", recipieStar); // Contact Phone
        values.put("reciepie_name", recipieName);
        values.put("reciepie_description_head", recipieHead);
        values.put("reciepie_description_full", recipieFull);

        // updating row
        return db.update("reciepie", values, "auto_id" + " = ?",
                new String[] { String.valueOf(id) });
    }
    public int updateRecipie(String id,String recipieStar) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("reciepie_star", recipieStar); // Contact Phone

        // updating row
        return db.update("reciepie", values, "auto_id" + " = ?",
                new String[] { String.valueOf(id) });
    }
    public void deleteRecipie(String id) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete("reciepie", "auto_id" + " = ?",
                new String[] { String.valueOf(id) });
        db.close();
    }

    public List<RecipieDataModel> getAllRecipie() {
        List<RecipieDataModel> contactList = new ArrayList<RecipieDataModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM reciepie";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor == null)
            return  null;
        if (cursor.getCount() == 0){
            return  null;
        }
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                RecipieDataModel contact = new RecipieDataModel(cursor.getString(0),cursor.getString(1),cursor.getString(2),
                        cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6));

                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }
    public List<RecipieDataModel> getAllRecipie(String search) {
        List<RecipieDataModel> contactList = new ArrayList<RecipieDataModel>();
        // Select All Query

        String selectQuery = "SELECT  * FROM reciepie";
        if (!search.trim().equals("")){
           // selectQuery = "SELECT  * FROM reciepie WHERE reciepie_name LIKE '%" + search + "%'";
            selectQuery = "SELECT  * FROM reciepie WHERE reciepie_name LIKE '%" + search + "%' OR reciepie_description_full LIKE '%" + search + "%'";
        }
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor == null)
            return  null;
        if (cursor.getCount() == 0){
            return  null;
        }
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                RecipieDataModel contact = new RecipieDataModel(cursor.getString(0),cursor.getString(1),cursor.getString(2),
                        cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6));

                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }
    public List<RecipieDataModel> getAllRecipieByMobileNumber(String mobileNumber) {
        List<RecipieDataModel> contactList = new ArrayList<RecipieDataModel>();
        // Select All Query
        String selectQuery = "SELECT  * FROM reciepie WHERE user_mobileNumber='" + mobileNumber + "'";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);
        if (cursor == null)
            return  null;
        if (cursor.getCount() == 0){
            return  null;
        }
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                RecipieDataModel contact = new RecipieDataModel(cursor.getString(0),cursor.getString(1),cursor.getString(2),
                        cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6));

                // Adding contact to list
                contactList.add(contact);
            } while (cursor.moveToNext());
        }

        // return contact list
        return contactList;
    }

    void createUser(String userMobileNumber,String userPassword,String userName) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("user_mobileNumber", userMobileNumber); // Contact Name
        values.put("user_password", userPassword); // Contact Phone
        values.put("user_name", userName);

        db.insert("login_data", null, values);
        //2nd argument is String containing nullColumnHack
        db.close(); // Closing database connection
    }
    public int updateUser(String userMobileNumber,String userPassword,String userName) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("user_password", userPassword); // Contact Name
        values.put("user_name", userName); // Contact Phone


        // updating row
        return db.update("login_data", values, "user_mobileNumber" + " = ?",
                new String[] { String.valueOf(userMobileNumber) });
    }
    UserDataModel getUser(String mobileNumber,String userPassword) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM login_data WHERE user_mobileNumber='" + mobileNumber + "' AND user_password='" + userPassword + "'";

        Cursor cursor = db.rawQuery(selectQuery,null);
        if (cursor == null)
            return  null;
        if (cursor.getCount() == 0){
            return  null;
        }
        if (cursor != null)
            cursor.moveToFirst();

        UserDataModel contact = new UserDataModel(cursor.getString(0),
                cursor.getString(1), cursor.getString(2));


        // return contact
        return contact;
    }

    UserDataModel getUser(String mobileNumber) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT  * FROM login_data WHERE user_mobileNumber='" + mobileNumber + "'";

        Cursor cursor = db.rawQuery(selectQuery,null);
        if (cursor == null)
            return  null;
        if (cursor.getCount() == 0){
            return  null;
        }
        if (cursor != null)
            cursor.moveToFirst();

        UserDataModel contact = new UserDataModel(cursor.getString(0),
                cursor.getString(1), cursor.getString(2));


        // return contact
        return contact;
    }


}
