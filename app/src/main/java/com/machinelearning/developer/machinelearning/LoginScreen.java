package com.machinelearning.developer.machinelearning;

import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class LoginScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_screen);

        TextView signUpButton = (TextView) findViewById(R.id.textView5);
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent =  new Intent(LoginScreen.this,SignupScreen.class);
                LoginScreen.this.startActivity(intent);
            }
        });

        final EditText mobileNumber = (EditText)findViewById(R.id.editText);
        final EditText password = (EditText)findViewById(R.id.editText2);
        Button bt = (Button)findViewById(R.id.button);
        bt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(mobileNumber.getText().toString().trim().equals("")){
                    Toast.makeText(LoginScreen.this,"Mobile Number is Empty",Toast.LENGTH_SHORT).show();
                }
                else if(password.getText().toString().trim().equals("")){
                    Toast.makeText(LoginScreen.this,"password  is Empty",Toast.LENGTH_SHORT).show();
                }else {
                    DatabaseHandler db = new DatabaseHandler(LoginScreen.this);
                    UserDataModel userDataModel = db.getUser(mobileNumber.getText().toString().trim(), password.getText().toString().trim());
                    if(userDataModel == null){
                        Toast.makeText(LoginScreen.this,"Login Failed",Toast.LENGTH_SHORT).show();
                    }else {
                        SharedPreferences.Editor editor = LoginScreen.this.getSharedPreferences(AppConstant.PREFERANCE_NAME,MODE_PRIVATE).edit();
                        editor.putString(AppConstant.USER_MOBILE_NUMBER,mobileNumber.getText().toString().trim());
                        editor.putString(AppConstant.USER_NAME,userDataModel.userName);
                        editor.apply();
                        AppConstant.myUserMobileNumber = mobileNumber.getText().toString();
                        AppConstant.user_name = userDataModel.userName;
                        Intent intent = new Intent(LoginScreen.this, MainActivity.class);
                        LoginScreen.this.startActivity(intent);
                    }

                }
            }
        });

    }
}
