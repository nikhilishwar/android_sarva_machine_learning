package com.machinelearning.developer.machinelearning;

import android.content.Context;
import android.content.Intent;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import java.io.File;
import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {
    List<ListItem> listItem;
    Context context;
    public MyAdapter(List<ListItem> listItem,Context context){
        this.listItem = listItem;
        this.context = context;
    }

    void updateAdapter(List<ListItem> listItem){
        this.listItem = listItem;
        notifyDataSetChanged();
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.list_item, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        final ListItem listItem = this.listItem.get(i);
        viewHolder.textViewHead.setText(listItem.head);
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File(sdCard.getAbsolutePath() + "/FoodCourt");
        String fileName  = listItem.id.trim() + ".jpg" ;
        File outFile = new File(dir, fileName);
        if(outFile.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(outFile.getAbsolutePath());
            viewHolder.imageView.setImageBitmap(myBitmap);
        }

        viewHolder.textViewDescription.setText(listItem.description);
        viewHolder.starRating.setRating(Float.parseFloat(listItem.star));
        viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(context,ViewRecipie.class);
                AppConstant.cachedListItem = listItem;
                context.startActivity(i);
            }
        });
    }

    @Override
    public int getItemCount() {
        return listItem.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewHead, textViewDescription;
        public ImageView imageView;
        public RatingBar starRating;
        public LinearLayout linearLayout;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            textViewHead = (TextView) itemView.findViewById(R.id.textView3);
            textViewDescription = (TextView) itemView.findViewById(R.id.textView6);
            imageView = (ImageView) itemView.findViewById(R.id.imageView3);
            starRating = (RatingBar) itemView.findViewById(R.id.ratingBar);
            linearLayout = (LinearLayout)itemView.findViewById(R.id.parentLayout);
        }
    }

}