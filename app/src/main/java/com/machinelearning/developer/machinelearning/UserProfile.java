package com.machinelearning.developer.machinelearning;

import androidx.appcompat.app.AppCompatActivity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class UserProfile extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
       final EditText name = (EditText)findViewById(R.id.editText3);
        final EditText password = (EditText)findViewById(R.id.editText3);
        Button onSubmit = (Button)findViewById(R.id.button6);
        onSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(name.getText().toString().trim().equals("")){
                    Toast.makeText(UserProfile.this,"Mobile Number is Empty",Toast.LENGTH_SHORT).show();
                }
                else if(password.getText().toString().trim().equals("")){
                    Toast.makeText(UserProfile.this,"Password is Empty",Toast.LENGTH_SHORT).show();
                }
                else{

                    DatabaseHandler db = new DatabaseHandler(UserProfile.this);
                    db.updateUser(AppConstant.myUserMobileNumber,password.getText().toString().trim(),name.getText().toString().trim());
                    UserProfile.this.finish();

                    SharedPreferences.Editor editor = UserProfile.this.getSharedPreferences(AppConstant.PREFERANCE_NAME,MODE_PRIVATE).edit();
                    editor.putString(AppConstant.USER_NAME,name.getText().toString().trim());
                    editor.apply();
                    AppConstant.user_name = name.getText().toString().trim();
                    AppConstant.mainActivity.tv.setText(AppConstant.user_name);
                }
            }
        });
    }
}
