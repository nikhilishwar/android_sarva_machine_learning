package com.machinelearning.developer.machinelearning;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class SignupScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup_screen);

        Button button = (Button)findViewById(R.id.button);
        final EditText mobileNumber = (EditText)findViewById(R.id.editText);
        final EditText name = (EditText)findViewById(R.id.editText5);
        final EditText password = (EditText)findViewById(R.id.editText2);
        final EditText rePassword = (EditText)findViewById(R.id.editText6);
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(mobileNumber.getText().toString().trim().equals("")){
                    Toast.makeText(SignupScreen.this,"Mobile Number is Empty",Toast.LENGTH_SHORT).show();
                }
                 else if(name.getText().toString().trim().equals("")){
                    Toast.makeText(SignupScreen.this,"Mobile Number is Empty",Toast.LENGTH_SHORT).show();
                }
                else if(password.getText().toString().trim().equals("")){
                    Toast.makeText(SignupScreen.this,"Mobile Number is Empty",Toast.LENGTH_SHORT).show();
                }
                else if(rePassword.getText().toString().trim().equals("")){
                    Toast.makeText(SignupScreen.this,"Mobile Number is Empty",Toast.LENGTH_SHORT).show();
                }
                else if(!rePassword.getText().toString().trim().equals(password.getText().toString().trim())){
                    Toast.makeText(SignupScreen.this,"Mobile Number is Empty",Toast.LENGTH_SHORT).show();
                }else{
                    DatabaseHandler db = new DatabaseHandler(SignupScreen.this);
                    db.createUser(mobileNumber.getText().toString().trim(),password.getText().toString().trim(),name.getText().toString().trim());
                    SignupScreen.this.finish();
                }
            }
        });
    }
}
