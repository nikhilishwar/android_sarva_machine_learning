package com.machinelearning.developer.machinelearning;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.github.dhaval2404.imagepicker.ImagePicker;
import com.github.dhaval2404.imagepicker.ImagePickerActivity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class AddRecipie extends AppCompatActivity  {

    ImageButton imageButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_recipie);
        Button btnback = (Button)findViewById(R.id.button2);
        final EditText reccipieName = (EditText)findViewById(R.id.editText4);
        final EditText headerText = (EditText)findViewById(R.id.editText9);
        final EditText fullText = (EditText)findViewById(R.id.editText10);
        imageButton = (ImageButton) findViewById(R.id.imageButton);

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              new ImagePicker.Builder(AddRecipie.this).crop(1f, 1f)	    		//Crop Square image(Optional)
                        .compress(1024)			//Final image size will be less than 1 MB(Optional)
                        .maxResultSize(620, 620)	//Final image resolution will be less than 620 x 620(Optional)
                        .start();
            }
        });

        Button btnSave =(Button)findViewById(R.id.button3);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(reccipieName.getText().toString().trim().equals("")){
                    Toast.makeText(AddRecipie.this,"Mobile Number is Empty",Toast.LENGTH_SHORT).show();
                }
                else if(headerText.getText().toString().trim().equals("")){
                    Toast.makeText(AddRecipie.this,"Mobile Number is Empty",Toast.LENGTH_SHORT).show();
                }
                else if(fullText.getText().toString().trim().equals("")){
                    Toast.makeText(AddRecipie.this,"Mobile Number is Empty",Toast.LENGTH_SHORT).show();
                }
                else if(imageButton.getDrawable() == null){
                    Toast.makeText(AddRecipie.this,"Pick Your Image",Toast.LENGTH_SHORT).show();
                }
                else{
                    DatabaseHandler db = new DatabaseHandler(AddRecipie.this);

                   String str = db.createRecipie(AppConstant.myUserMobileNumber,"1",reccipieName.getText().toString().trim(),
                            headerText.getText().toString().trim(),fullText.getText().toString().trim(),AppConstant.user_name);
                    AppConstant.cachedRecipieFragment.refresh();
                    saveImage(str);
                    AddRecipie.this.finish();
                }
            }
        });

        btnback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AddRecipie.this.finish();
            }
        });
    }

    void saveImage(String string){
        FileOutputStream outStream = null;

        // Write to SD Card
        try {
            BitmapDrawable drawable = (BitmapDrawable) imageButton.getDrawable();
            Bitmap bitmap = drawable.getBitmap();
            File sdCard = Environment.getExternalStorageDirectory();
            File dir = new File(sdCard.getAbsolutePath() + "/FoodCourt");
            dir.mkdirs();

            String fileName = string.trim() + ".jpg" ;
            File outFile = new File(dir, fileName);

            outStream = new FileOutputStream(outFile);
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, outStream);
            outStream.flush();
            outStream.close();



        } catch (FileNotFoundException e) {

            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        }
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            //Image Uri will not be null for RESULT_OK
            Uri fileUri = data.getData();

            imageButton.setImageURI(fileUri);
            //You can get File object from intent

        } else if (resultCode == ImagePicker.RESULT_ERROR) {
            Toast.makeText(this, "Error Picing Image", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(this, "Task Cancelled", Toast.LENGTH_SHORT).show();
        }
    }


}
