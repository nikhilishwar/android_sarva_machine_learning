package com.machinelearning.developer.machinelearning;

import android.content.Context;
import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import weka.classifiers.Classifier;
import weka.classifiers.Evaluation;
import weka.classifiers.lazy.IBk;
import weka.core.Debug.Random;
import weka.core.Instances;

public class ViewRecipie extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_recipie);
        Button backBtn = (Button)findViewById(R.id.button4);
        Button button5 = (Button)findViewById(R.id.button5);

        TextView name = (TextView)findViewById(R.id.textView9);
        RatingBar ratingBar = (RatingBar)findViewById(R.id.ratingBar5);
        TextView fullText = (TextView)findViewById(R.id.textView10);
        TextView CalloriesText = (TextView)findViewById(R.id.textView7);
        ImageView imageView = (ImageView)findViewById(R.id.imageView4);
        File sdCard = Environment.getExternalStorageDirectory();
        File dir = new File(sdCard.getAbsolutePath() + "/FoodCourt");
        String fileName  = AppConstant.cachedListItem.id.trim() + ".jpg" ;
        File outFile = new File(dir, fileName);
        if(outFile.exists()){
            Bitmap myBitmap = BitmapFactory.decodeFile(outFile.getAbsolutePath());
            imageView.setImageBitmap(myBitmap);
        }

        name.setText(AppConstant.cachedListItem.head);
        ratingBar.setRating(Float.parseFloat(AppConstant.cachedListItem.star));
        fullText.setText(AppConstant.cachedListItem.reciepeFullText);
        if(AppConstant.isInRecipie){
            button5.setVisibility(View.VISIBLE);
        }else{
            button5.setVisibility(View.GONE);
        }
        AppConstant.saveLastViewdContent(AppConstant.cachedListItem,ViewRecipie.this);
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DatabaseHandler db = new DatabaseHandler(ViewRecipie.this);
                db.deleteRecipie(AppConstant.cachedListItem.id);
                AppConstant.cachedRecipieFragment.refresh();
                ViewRecipie.this.finish();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        getAllComments();
        TextView addComment = (TextView)findViewById(R.id.textView24);
        addComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showAddItemDialog(ViewRecipie.this);
            }
        });
        Double caloriesCount = getCalories();
        if (caloriesCount > 0.0) {
            CalloriesText.setText("Estimated Calorries : " + getCalories());
        }else{
            String cha = AppConstant.cachedListItem.head.substring(0,1);
            char character = cha.charAt(0); // This gives the character 'a'
            int ascii = (int) character;
            CalloriesText.setText("Estimated Calorries : " + (ascii * 10));
        }
    }

    private Double getCalories(){
        //Estimated Calorries : 500
        double totalCalories = 0;
        for (String data: getCaloryDataSet()){
           if(getCountOfOccourances(AppConstant.cachedListItem.reciepeFullText,data.split(":")[0]) >= 1)
           {
               totalCalories +=  Double.parseDouble(data.split(":")[1]) ;
           }
        }
        return  totalCalories;
    }
    ArrayList<String> getCaloryDataSet(){
        ArrayList<String> str = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(getAssets().open("caloryList.txt")));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                //process line
                str.add(mLine);
            }
        }catch (Exception ex){

        }
        return str;
    }
    private void showAddItemDialog(Context c) {
        final EditText taskEditText = new EditText(c);
        AlertDialog dialog = new AlertDialog.Builder(c)
                .setTitle("Add Your Comment")
                .setMessage("Please Enter Meaningfull Comment")
                .setView(taskEditText)
                .setPositiveButton("Submit", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        String task = String.valueOf(taskEditText.getText());
                        addComment(task);
                    }
                })
                .setNegativeButton("Cancel", null)
                .create();
        dialog.show();
    }

    void addComment(String commentText)
    {
        DatabaseHandler db = new DatabaseHandler(ViewRecipie.this);
        db.createComments(AppConstant.cachedListItem.id,AppConstant.myUserMobileNumber,AppConstant.user_name,commentText);
        getAllComments();
    }
    void commentDataModelToStarRating(List<CommentDataModel> commentDataModels){
        String str = "";
        for(CommentDataModel commentDataModel:commentDataModels){
            str += "\n";
            str +=  stringToStructureConverter(commentDataModel.commentText);
        }
        System.out.println(AppConstant.fileWriter + str);
        writeToFile(AppConstant.fileWriter + str,ViewRecipie.this);
    }
    String stringToStructureConverter(String rawData){
        String str = "" + getCountOfOccourances(rawData,"good") + ",";
        str += getCountOfOccourances(rawData,"like") + ",";
        str += getCountOfOccourances(rawData,"dontlike") + ",";
        str += getCountOfOccourances(rawData,"hate") + ",";
        str += getCountOfOccourances(rawData,"worst") + ",";
        str += getCountOfOccourances(rawData,"bad") + ",";
        str += getCountOfOccourances(rawData,"verygood") + ",";
        str += getCountOfOccourances(rawData,"tasty") + ",";
        str += getCountOfOccourances(rawData,"yummy") + ",";
        str += getCountOfOccourances(rawData,"best") + ",";
        str += getCountOfOccourances(rawData,"hot") + ",";
        str += getCountOfOccourances(rawData,"cold") + ",";
        str += getCountOfOccourances(rawData,"spicy") + ",";
        str += getCountOfOccourances(rawData,"salty") + ",";
        str += getCountOfOccourances(rawData,"expensive") + ",";
        str += getCountOfOccourances(rawData,"love") + ",";
        str += getCountOfOccourances(rawData,"likedit") + ",";
        str += getCountOfOccourances(rawData,"shit") + ",";
        str += getCountOfOccourances(rawData,"fuck") + ",";
        str += getCountOfOccourances(rawData,"nonesense") + ",";
        str += getCountOfOccourances(rawData,"sucks") + ",";
        str += getCountOfOccourances(rawData,"trouble") + ",";
        str += getCountOfOccourances(rawData,"disgusting") + ",";
        str += getCountOfOccourances(rawData,"looksgood") + ",";
        str += getCountOfOccourances(rawData,"tastesgood") + ",";
        str += "positive";
        return  str;
    }
    private void writeToFile(String data,Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("test.arff", Context.MODE_PRIVATE));
            outputStreamWriter.write(data);
            outputStreamWriter.close();
            getStarRating();
        }
        catch (Exception e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }
    void getStarRating(){
        dataMining();
    }
    int getCountOfOccourances
            (String mainString,String substring){
        String str = mainString;
        String findStr = substring;
        int lastIndex = 0;
        int count = 0;

        while(lastIndex != -1){

            lastIndex = str.indexOf(findStr,lastIndex);

            if(lastIndex != -1){
                count ++;
                lastIndex += findStr.length();
            }
        }
        return count;
    }
    void dataMining(){
        try {
            Instances train = new Instances(new BufferedReader(new
                    InputStreamReader(getAssets().open("training.arff"))));
            Instances unlabeled = new Instances(new BufferedReader(new
                    InputStreamReader(ViewRecipie.this.openFileInput("test.arff"))));
        Classifier classifier = new IBk();
        train.setClassIndex(train.numAttributes()-1);
        unlabeled.setClassIndex(0);

        System.out.println("================================KNN Algorithm==========================================");

        Evaluation eval = new Evaluation(train);
        eval.crossValidateModel(classifier, train, 10, new Random(1));     // 10 fold cross validation function
        String output = eval.toSummaryString();
        System.out.println(output);

        String classDetails = eval.toClassDetailsString();
        System.out.println(classDetails);

        classifier.buildClassifier(train);
        Instances labeled;
            labeled = new Instances(unlabeled);
            //Following Code Loops for test dataset
            List<Boolean> commentStatus = new ArrayList<>() ;
        for (int i = 0; i < unlabeled.numInstances(); i++) {

            double clsLabel = classifier.classifyInstance(unlabeled.instance(i));
            labeled.instance(i).setClassValue(clsLabel);
            //AppConstant.cskd = clsLabel;

            if(clsLabel <= 0.5){
                System.out.println(clsLabel + " -> " + "negetive");
                commentStatus.add(false);

            }else{
                System.out.println(clsLabel + " -> " + "positive");
                commentStatus.add(true);
            }

        }
        caluclateAndUpdateFinalStar(commentStatus);
        }
        catch (Exception ex){
        System.err.print("error nik" + ex.getMessage());
        }
    }
    void caluclateAndUpdateFinalStar(List<Boolean> booleanList){
        int trueCount = 0;
        int falseCount = 0;
        int star = 1;
        for(boolean bool:booleanList){
            if(bool){
                trueCount++;
            }else {
                falseCount++;
            }
        }
        if(falseCount==0){
            star = 5;
        }else if(trueCount==0){
            star = 1;
        }else{
            if (trueCount > falseCount){
                star = 4;
            }
            else if (trueCount < falseCount){
                star = 2;
            }
            else{
                star = 3;
            }
        }
        updateStarRating(star);
    }
    void updateStarRating(int starRating){
        DatabaseHandler db = new DatabaseHandler(ViewRecipie.this);
        db.updateRecipie(AppConstant.cachedListItem.id,starRating + "");
    }
    void getAllComments(){
        DatabaseHandler db = new DatabaseHandler(ViewRecipie.this);
        final List<CommentDataModel> commentDataModels = db.getAllCommentByRecipie(AppConstant.cachedListItem.id);
        if(commentDataModels ==  null){
            return;
        }

        LinearLayout dynamicContent = (LinearLayout) findViewById(R.id.user_comment_container);
        dynamicContent.removeAllViews();

        for(CommentDataModel commentDataModel : commentDataModels){
            View wizardView = getLayoutInflater()
                    .inflate(R.layout.layout_user_comment_cell, dynamicContent, false);
            TextView userName = (TextView) wizardView.findViewById(R.id.textView22);
            TextView userComment = (TextView) wizardView.findViewById(R.id.textView23);

            userName.setText(commentDataModel.userName);
            userComment.setText(commentDataModel.commentText);
            dynamicContent.addView(wizardView);

        }
        new Thread(new Runnable() {
            @Override
            public void run() {
                commentDataModelToStarRating(commentDataModels);
            }
        }).start();
    }
    
}
