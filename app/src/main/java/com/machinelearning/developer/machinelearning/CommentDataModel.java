package com.machinelearning.developer.machinelearning;

public class CommentDataModel {
    String idOfReciepe;
    String userMobileNumber;
    String userName;
    String commentText;
    String commentId;

    CommentDataModel(){
        commentId = "";
        idOfReciepe = "";
        userMobileNumber = "";
        userName = "";
        commentText = "";

    }
    CommentDataModel(String commentId,String idOfReciepe,String userMobileNumber,String userName,String commentText){
        this.commentId = commentId;
        this.idOfReciepe = idOfReciepe;
        this.userMobileNumber = userMobileNumber;
        this.userName = userName;
        this.commentText = commentText;

    }


}

