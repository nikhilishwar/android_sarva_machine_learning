package com.machinelearning.developer.machinelearning;

import android.os.Bundle;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ReccomondedFragment extends Fragment {
    private RecyclerView recyclerView;
    private MyAdapter adapter;
    private List<ListItem> listItem;
    View view;;
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v =  inflater.inflate(R.layout.fragment_recomonded,container,false);

        view = v;

        DatabaseHandler db = new DatabaseHandler(v.getContext());
        List<RecipieDataModel> allRecipie = db.getAllRecipie();
        recyclerView = (RecyclerView)v.findViewById(R.id.recyclerviewrec);
        recyclerView.setVisibility(View.VISIBLE);

        if (allRecipie == null){
            recyclerView.setVisibility(View.GONE);
            return  v;
        }
        listItem = new ArrayList<>();
//        for (RecipieDataModel recipieModel: allRecipie){
//            ListItem listIte = new ListItem(recipieModel.recipieName,recipieModel.recipieStar,recipieModel.recipieHead,
//                    recipieModel.recipieFull,"no",recipieModel.recipieId);
//            listItem.add(listIte);
//        }
        listItem = getRecomendedProducts(allRecipie);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(v.getContext()));
        adapter = new MyAdapter(listItem,v.getContext());

        recyclerView.setAdapter(adapter);




        return v;
    }

    List<ListItem> getRecomendedProducts(List<RecipieDataModel> recipieDataModels){
        ListItem item  = AppConstant.getLastViewdContent(view.getContext());
         List<ListItem> locallistItem;
         locallistItem = new ArrayList<>();
        if (item == null){
            if (recipieDataModels.size() <= 2){

                for (RecipieDataModel recipieModel: recipieDataModels){
                    ListItem listIte = new ListItem(recipieModel.recipieName,recipieModel.recipieStar,recipieModel.recipieHead,
                            recipieModel.recipieFull,"no",recipieModel.recipieId);
                    locallistItem.add(listIte);

                }
                return locallistItem;
            }
            else{
                int cnt=0;
                for (RecipieDataModel recipieModel: recipieDataModels){
                    ListItem listIte = new ListItem(recipieModel.recipieName,recipieModel.recipieStar,recipieModel.recipieHead,
                            recipieModel.recipieFull,"no",recipieModel.recipieId);
                    locallistItem.add(listIte);
                    cnt++;
                    if(cnt == 2){
                        break;
                    }
                }
                return locallistItem;
            }
        }
        if (recipieDataModels.size() <= 2){

            for (RecipieDataModel recipieModel: recipieDataModels){
                ListItem listIte = new ListItem(recipieModel.recipieName,recipieModel.recipieStar,recipieModel.recipieHead,
                        recipieModel.recipieFull,"no",recipieModel.recipieId);
                locallistItem.add(listIte);

            }
            return locallistItem;
        }
        ListItem max,secondMax;
        double maxs=0,secondMaxs=0;
        max = new ListItem(recipieDataModels.get(0).recipieName, recipieDataModels.get(0).recipieStar, recipieDataModels.get(0).recipieHead,
                recipieDataModels.get(0).recipieFull, "no", recipieDataModels.get(0).recipieId);
        secondMax = new ListItem(recipieDataModels.get(0).recipieName, recipieDataModels.get(0).recipieStar, recipieDataModels.get(0).recipieHead,
                recipieDataModels.get(0).recipieFull, "no", recipieDataModels.get(0).recipieId);
        for (RecipieDataModel recipieModel: recipieDataModels) {
            ListItem listIte = new ListItem(recipieModel.recipieName, recipieModel.recipieStar, recipieModel.recipieHead,
                    recipieModel.recipieFull, "no", recipieModel.recipieId);



            CosainSimilarity cm = new CosainSimilarity();
            Map<CharSequence, Integer> hm =
                    new HashMap<CharSequence, Integer>();

            for(String str : startCosineAlgorithm()){
                hm.put(str, new Integer(getCountOfOccourances(listIte.reciepeFullText,str)));
            }
            Map<CharSequence, Integer> hms =
                    new HashMap<CharSequence, Integer>();

            for(String str : startCosineAlgorithm()){
                hms.put(str, new Integer(getCountOfOccourances(item.reciepeFullText,str)));
            }
            Double dbs = cm.cosineSimilarity(hm, hms);
           // Toast.makeText(view.getContext(),"" + dbs,Toast.LENGTH_LONG).show();
            //locallistItem.add(listIte);
            if(dbs >= maxs){
                secondMaxs = maxs;
                secondMax = max;
                maxs = dbs;
                max = listIte;
            }else if(dbs >= secondMaxs){
                secondMaxs = dbs;
                secondMax = listIte;
            }

        }
        locallistItem.add(max);
        locallistItem.add(secondMax);
        return locallistItem;
    }
    ArrayList<String> startCosineAlgorithm(){
        ArrayList<String> str = new ArrayList<>();
        try {
            BufferedReader reader = new BufferedReader(
                    new InputStreamReader(view.getContext().getAssets().open("similarityDataSet.txt")));

            // do reading, usually loop until end of file reading
            String mLine;
            while ((mLine = reader.readLine()) != null) {
                //process line
                str.add(mLine);
            }
        }catch (Exception ex){

        }
        return str;
    }

    int getCountOfOccourances(String mainString,String substring){
        String str = mainString;
        String findStr = substring;
        int lastIndex = 0;
        int count = 0;

        while(lastIndex != -1){

            lastIndex = str.indexOf(findStr,lastIndex);

            if(lastIndex != -1){
                count ++;
                lastIndex += findStr.length();
            }
        }
        return count;
    }
    void refresh() {

        DatabaseHandler db = new DatabaseHandler(view.getContext());
        List<RecipieDataModel> allRecipie = db.getAllRecipieByMobileNumber(AppConstant.myUserMobileNumber);
        recyclerView = (RecyclerView)view.findViewById(R.id.recyclerviewrec);
        recyclerView.setVisibility(View.VISIBLE);
        if (allRecipie == null){
            recyclerView.setVisibility(View.GONE);
            return;
        }
        if (listItem == null){
            listItem = new ArrayList<>();
        }
        listItem.clear();

        for (RecipieDataModel recipieModel: allRecipie){
            ListItem listIte = new ListItem(recipieModel.recipieName,recipieModel.recipieStar,recipieModel.recipieHead,
                    recipieModel.recipieFull,"no",recipieModel.recipieId);
            listItem.add(listIte);
        }
        if (adapter == null){

            recyclerView.setHasFixedSize(true);
            recyclerView.setLayoutManager(new LinearLayoutManager(view.getContext()));
            adapter = new MyAdapter(listItem,view.getContext());

            recyclerView.setAdapter(adapter);

            return;
        }
        adapter.updateAdapter(listItem);
    }
}
