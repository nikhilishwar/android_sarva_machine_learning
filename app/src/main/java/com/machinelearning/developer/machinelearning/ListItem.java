package com.machinelearning.developer.machinelearning;

public class ListItem {
     String head;
     String star;
     String description;
     String reciepeFullText;
     String image;

     String id;
    ListItem(String head,String star,String description,String reciepeFullText,String image,String id){
        this.head = head;
        this.star = star;
        this.description = description;
        this.reciepeFullText = reciepeFullText;
        this.image = image;
        this.id = id;
    }
    ListItem(){
        this.head = "";
        this.star = "";
        this.description = "";
        this.reciepeFullText = "";
        this.image = "";
        this.id = "";
    }
}
