package com.machinelearning.developer.machinelearning;

public class RecipieDataModel {
    String recipieId;
    String userMobileNumber;
    String recipieStar;
    String recipieName;
    String recipieHead;
    String recipieFull;

    String userName;
    RecipieDataModel(){
        recipieId = "";
        userMobileNumber = "";
        recipieStar = "";
        recipieName = "";
        recipieHead = "";
        recipieFull = "";
        userName = "";
    }
    RecipieDataModel( String recipieId,String userMobileNumber,String recipieStar,String recipieName,String recipieHead,String recipieFull,String userName){
        this.recipieId = recipieId;
        this.userMobileNumber = userMobileNumber;
        this.recipieStar = recipieStar;
        this.recipieName = recipieName;
        this.recipieHead = recipieHead;
        this.recipieFull = recipieFull;
        this.userName =  userName;
    }
}
