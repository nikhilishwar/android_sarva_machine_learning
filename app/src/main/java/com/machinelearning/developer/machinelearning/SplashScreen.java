package com.machinelearning.developer.machinelearning;

import android.content.Intent;
import android.content.SharedPreferences;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public class SplashScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        SharedPreferences editor = getSharedPreferences(AppConstant.PREFERANCE_NAME,MODE_PRIVATE);
        final String userName = editor.getString(AppConstant.USER_MOBILE_NUMBER,null);
        final String userNotMobile = editor.getString(AppConstant.USER_NAME,null);
         new Thread(new Runnable() {
            @Override
            public void run() {
                try{
                    Thread.sleep(3000);
                    if(userName == null){
                        Intent intent = new Intent(SplashScreen.this,LoginScreen.class);
                        SplashScreen.this.startActivity(intent);
                    }
                    else {
                        if(userName.trim() == ""){
                            Intent intent = new Intent(SplashScreen.this,LoginScreen.class);
                            SplashScreen.this.startActivity(intent);
                        }
                        else{
                            AppConstant.myUserMobileNumber = userName;
                            AppConstant.user_name = userNotMobile;
                            Intent intent = new Intent(SplashScreen.this,MainActivity.class);
                            SplashScreen.this.startActivity(intent);
                        }
                    }

                }
                catch (Exception ex){

                }
            }
        }).start();
    }
}
