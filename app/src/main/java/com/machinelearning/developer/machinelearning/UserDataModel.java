package com.machinelearning.developer.machinelearning;

public class UserDataModel {
    String userMobileNumber;
    String userPassword;
    String userName;

    UserDataModel(){
        userMobileNumber = "";
        userPassword = "";
        userName = "";
    }
    UserDataModel(String userMobileNumber,String userPassword,String userName){
        this.userMobileNumber = userMobileNumber;
        this.userPassword = userPassword;
        this.userName = userName;
    }
}
